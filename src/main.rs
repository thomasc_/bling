use colored::*;
use std::io;
use rand::Rng;

const COLORS: [&str; 8] = [
    "black", "white", "red", "yellow", "green", "cyan", "blue", "magenta",
];
fn main() {
    println!("Welcome to Rusted Colors!\n");
    display_bling(select_bling()); 

}

fn square1() {
    for i in 0..8 {
        let char = String::from("██").color(COLORS[i]);
        print!("{} ", char);
    }
    print!("\n");
    for i in 0..8 {
        let char = String::from("██").color(COLORS[i]).dimmed();
        print!("{} ", char);
    }
    print!("\n\n");
}

fn panes() {
    let char0 = "█";
    let char1 = "▄";
    let char2 = "▀";
    let char3 = " ";

    let seg0 = [char0, char0, char0, char1];
    let seg1 = [char0, char0, char0, char0];
    let seg2 = [char0, char0, char0, char0];
    let seg3 = [char3, char2, char2, char2];
    let shape = [seg0, seg1, seg2, seg3];

    for i in 0..3 {
        let seg = shape[i];
        for i in 0..8 {
            print!(
                "{}{}{}{} ",
                seg[0].color(COLORS[i]),
                seg[1].color(COLORS[i]),
                seg[2].color(COLORS[i]),
                seg[3].color(COLORS[i]).dimmed()
            )
        }
        print!("\n")
    }
    for i in 0..8 {
        print!(
            "{}{}{}{} ",
            seg3[0].color(COLORS[i]).dimmed(),
            seg3[1].color(COLORS[i]).dimmed(),
            seg3[2].color(COLORS[i]).dimmed(),
            seg3[3].color(COLORS[i]).dimmed()
        )
    }
    print!("\n\n")
}


fn select_bling() -> usize {
    // choices.push("Should be here");

    let mut buffer = String::new();
    println!("Please input selection or press enter for random.");
    let selection = match io::stdin().read_line(&mut buffer) {
        Ok(_) => {
            buffer
        }
        Err(_) => panic!("Invalid input.")

    };

    return selection.trim().parse::<usize>().unwrap_or(0);

    
    
}

fn display_bling (sel:usize) {
    print!("\x1B[2J\x1B[1;1H\n\n");
    let mut choices = Vec::new();
    choices.push(square1 as fn());
    choices.push(panes as fn());

    if sel == 0 {
         let mut rand = rand::thread_rng();
         choices[rand.gen_range(0..choices.len())]();
    }
    else {choices[sel-1]();}

    
    
}
